### wandr

Wandr is a web-app made with React where players look for hidden QR codes hidden
around the city. Scanning the codes rewards the players with points. Players are
incentivized to find codes that have been found by very few players, because
the less discoveries a QR has, the more points the player will receive for
finding it.


This game was largely inspired by the overwhelming success of Pokemon Go.
Unfortunately I don't have nearly as many resources as Niantic and Google, so
I had to get creative with QR codes.


This was meant to only be a semester long project just so I can see what could
be done with an idea like this. By the end of the semester there were ~650 codes
scanned. Admittidely about 100 of the scans were me testing the game, but that
leaves about 500 people that have played the game, which is a success to me.

While you can't play the game if you can't scan any of the codes, you can still
visit the website and get your first "discovery" at: 

[wandr.land](wandr.land)